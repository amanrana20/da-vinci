%% Setup
clc
clear all
close all

%% Initializing
syms q1 q2 q3 q4 q5 q6 q7 q8
L_arm = 279.4; % mm
L_forearm = 304.8; %mm
h = 150.6; % mm

% DH Parameters
a = [0, L_arm, L_forearm, 0, 0, 0, 0];
alpha = [90, 0, -90, 90, -90, 90, 0];
d = [0, 0, 0, h, 0, 0, 0];
theta = [-pi/2+q1, -pi/2+q2, pi/2+q3, q4, q5, -pi/2+q6, pi/2+q7];


%% Calculations
% Transformation Matrices
T10 = get_transformation_matrix(theta(1), d(1), a(1), alpha(1));
T10 = simplify(T10);
display(T10);
T10_numeric = subs(T10, q1, 0);

T21 = get_transformation_matrix(theta(2), d(2), a(2), alpha(2));
T21 = simplify(T21);
T20 = T10 * T21;
display(T20);
T20_numeric = subs(T20, [q1, q2], [0, 0]);

T32 = get_transformation_matrix(theta(3), d(3), a(3), alpha(3));
T32 = simplify(T32);
T30 = T20 * T32;
display(T30);
T30_numeric = subs(T30, [q1, q2, q3], [0, 0, 0]);

T43 = get_transformation_matrix(theta(4), d(4), a(4), alpha(4));
T43 = simplify(T43);
T40 = T30 * T43;
display(T40);
T40_numeric = subs(T40, [q1, q2, q3, q4], [0, 0, 0, 0]);

T54 = get_transformation_matrix(theta(5), d(5), a(5), alpha(5));
T54 = simplify(T54);
T50 = T40 * T54;
% display(T50);
T50_numeric = subs(T50, [q1, q2, q3, q4, q5], [0, 0, 0, 0, 0]);

T65 = get_transformation_matrix(theta(6), d(6), a(6), alpha(6));
T65 = simplify(T65);
T60 = T50 * T65;
% display(T60);
T60_numeric = subs(T60, [q1, q2, q3, q4, q5, q6], [0, 0, 0, 0, 0, 0]);

T76 = get_transformation_matrix(theta(7), d(7), a(7), alpha(7));
T76 = simplify(T76);
T70 = T60 * T76;
% display(T70);
T70_numeric = subs(T70, [q1, q2, q3, q4, q5, q6, q7], [0, 0, 0, 0, 0, 0, 0]);
display(T70_numeric(1:3, 4));


%% Plotting the arm
% sending the values of Tn0 when all the algles are 0 degrees
plot_abb(T10_numeric, T20_numeric, T30_numeric, T40_numeric, T50_numeric, T60_numeric, T70_numeric);


%% Calculating Jacobian
J = calculate_jacobian(T30); % Pass the Tn0 matrices to calculate_jacobian function
J = simplify(J);
% J = subs(J, [q1, q2, q3, q4, q5, q6, q7], [0, 0, 0, 0, 0, 0, 0]);
% display(J);


%% Calculating inverse Jacobian
J_inv = inv(J);
J_inv = simplify(J_inv);
display(J_inv);

display('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -');
initial_position_of_end_effector = double(T30_numeric(1:3, 4));
display(initial_position_of_end_effector);

%% Desired end effector position input by user
display('Enter the desired position of the end effector.');
desired_posX = input('Enter desired x: ');
desired_posY = input('Enter desired y: ');
desired_posZ = input('Enter desired Z: ');
desired_posZ = desired_posZ - 150.6;
display(' ');
display('Inputs Stored. Processing...');

%% CALCULATE CHANGE IN THETA BASED ON CHANGE IN END EFFECTOR POSITION
% THEORY
% d_theta_dot = J_inverse* d_X_dot
% (where X = position of end effector & d_X is velocity of End Effector)
% MULTIPLYING BY d_t pon both sides
% d_theta = J_inverse* d_X

%%%% - - - - - - - - - - - - - - -

% TO FIND d_x, we will create small steps in the direction of the unit
% vector from the old to desired end effector position.

unit_vector = ([desired_posX; desired_posY; desired_posZ] - initial_position_of_end_effector) / ...
    ( (desired_posX - initial_position_of_end_effector(1))^2 + (desired_posY - initial_position_of_end_effector(2))^2 ...
    + (desired_posZ - initial_position_of_end_effector(3))^2 )^0.5;

% The value of the small step
step = double(unit_vector);

% Calculate the number of small steps
number_of_small_steps = (desired_posY - initial_position_of_end_effector(2)) / step(2);

% changing the variable name for ease of understanding and in accordance
% with the formula.
d_x = step;

% Initializing the value of q1, q2 and q3 to be 0 each.
angles = [0, 0, 0];

for i = 1:number_of_small_steps
    % Changing variable so that the original J_inv is preserved and can be
    % used again and again.
    J_inv1 = J_inv;
    J_inverse = double(subs(J_inv1, [q1, q2, q3], angles));
    
    % Using formula to calculate the change in angles due to change in
    % position of end effector.
    d_q = J_inverse * d_x;
    
    angles = double(angles + d_q');
    
    % all_angles contains angles calculated above plus the rest of the
    % angles (i.e. q4, q5,q6, q7) are all zero. This is done for the purpose of substitution. 
    all_angles = [angles, 0, 0, 0, 0];
    
    % Plotting
    T10_num = double(subs(T10, q1, all_angles(1)));
    T20_num = double(subs(T20, [q1, q2], all_angles(1:2)));
    T30_num = double(subs(T30, [q1, q2, q3], all_angles(1:3)));
    T40_num = double(subs(T40, [q1, q2, q3, q4], all_angles(1:4)));
    T50_num = double(subs(T50, [q1, q2, q3, q4, q5], all_angles(1:5)));
    T60_num = double(subs(T60, [q1, q2, q3, q4, q5, q6], all_angles(1:6)));
    T70_num = double(subs(T70, [q1, q2, q3, q4, q5, q6, q7], all_angles(1:7)));
    
    plot_da_vinci(T10_num(1:3, 4), T20_num(1:3, 4), T30_num(1:3, 4), T40_num(1:3, 4), T50_num(1:3, 4), T60_num(1:3, 4), T70_num(1:3, 4));
%     pause(1);
end