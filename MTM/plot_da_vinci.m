function [] = plot_da_vinci(T10_pos, T20_pos, T30_pos, T40_pos, T50_pos, T60_pos, T70_pos)
    % Matrix X containing X cooredinates, Y containing y coordinates and Z
    % containing z coordinates.
    X = int16([0, T10_pos(1), T20_pos(1), T30_pos(1), T40_pos(1), T50_pos(1), T60_pos(1), T70_pos(1)]);
    Y = [0, T10_pos(2), T20_pos(2), T30_pos(2), T40_pos(2), T50_pos(2), T60_pos(2), T70_pos(2)];
    Z = [0, T10_pos(3), T20_pos(3), T30_pos(3), T40_pos(3), T50_pos(3), T60_pos(3), T70_pos(3)];
    
    % plotting the line diagram
    hold on
    line(X, Y, Z, 'linewidth', 2);
    hold on
    
    for i = 1:8
        plot3(X(i), Y(i), Z(i), 'r*', 'linewidth', 2);
        hold on
    end
    
    hold on
    xlabel('X axis');
    ylabel('y axis');
    zlabel('z axis');
    view(30, 30)
    hold on
    
end