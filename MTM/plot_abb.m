%% This function plots the robot arms configuration in cartesian coordinates

function plot_abb(T01, T02, T03, T04, T05, T06, T07)
    % getting the cartesian coordinates of each joint
    T01_pos = T01(1:3,4);
    T02_pos = T02(1:3,4);
    T03_pos = T03(1:3,4);
    T04_pos = T04(1:3,4);
    T05_pos = T05(1:3,4);
    T06_pos = T06(1:3,4);
    T07_pos = T07(1:3,4);
    
    X = int16([0, T01_pos(1), T02_pos(1), T03_pos(1), T04_pos(1), T05_pos(1), T06_pos(1), T07_pos(1)]);
    Y = [0, T01_pos(2), T02_pos(2), T03_pos(2), T04_pos(2), T05_pos(2), T06_pos(2), T07_pos(2)];
    Z = [0, T01_pos(3), T02_pos(3), T03_pos(3), T04_pos(3), T05_pos(3), T06_pos(3), T07_pos(3)];
    
    % plotting the line diagram
    hold on
    line(X, Y, Z, 'linewidth', 2);
    hold on
    
    for i = 1:8
        plot3(X(i), Y(i), Z(i), 'r*', 'linewidth', 2);
        hold on
    end
    
    hold on
    xlabel('X axis');
    ylabel('y axis');
    zlabel('z axis');
    view(30, 30)
    hold on
end