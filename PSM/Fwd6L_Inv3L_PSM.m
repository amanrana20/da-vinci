%% FK for 6 links and IK for 3 links of PSM
% This file contains MATLAB code for determining the forward kinematics 
% for all 6 links of the Patient Side Manipulator (PSM) and inverse
% kinematics for the first 3 links of the PSM.
% Forward kinematics is implemented using DH parameters
% Inverse kinematics is implemented using the Pseudo-inverse-Jacobian
% technique which is based on an iterative solution (numerical methods).
% Change desired positions in variables xdx, xdy & xdz to check for 
% different configurations of end effector.
%% Initialization
clc;clear all;close all;
syms q1 q2 q3 q4 q5 q6 real;
lRcc = 0.4318;
lTool = 0.4162;
lP2Y = 0.0091;
lY2CP = 0.0102;
xdx = 0.8;
xdy = -0.8;
xdz = 0.2;
tstep = 0.01;
t = 0:tstep:1;
% Joint tracking initializations
jnt1tr = zeros(3,length(t));
jnt2tr = zeros(3,length(t));
jnt3tr = zeros(3,length(t));
jnt4tr = zeros(3,length(t));
jnt5tr = zeros(3,length(t));
jnt6tr = zeros(3,length(t));
jnt7tr = zeros(3,length(t));

%% DH Parameters
DH = [q1+pi/2,     0,         pi/2,      0;...
      q2-pi/2,     0,         -pi/2,     0;...
      0,           q3-lRcc,   pi/2,      0;...
      q4,          lTool,     0,         0;...
      q5-pi/2,     0,         -pi/2,     0;...
      q6-pi/2,     0,         -pi/2,     lP2Y;...
      0,           lY2CP,     -pi/2,     0];

%% Forward Kinematics
T_fin = eye(4); % Final transformation matrix (from base to tip)
for i = 1:size(DH,1)
    T(:,:,i) = dh2mat(DH(i,1),DH(i,2),DH(i,3),DH(i,4));
    T_fin = T_fin*T(:,:,i);
    Ti(:,i) = T(1:3,4);
end
T01 = T(:,:,1);
T02 = T01*T(:,:,2);
T03 = T02*T(:,:,3);
T04 = T03*T(:,:,4);
T05 = T04*T(:,:,5);
T06 = T05*T(:,:,6);
T07 = T06*T(:,:,7);
P_fin = T_fin(1:3,4); %Extracting only the x-y-z position from T matrix

%% Home configuration (0,0,0,0,0,0)
THome = subs(T_fin,[q1 q2 q3 q4 q5 q6],[0 0 0 0 0 0]);
PHome = THome(1:3,4);
display('Begining position:');
disp(vpa(PHome, 2));
    
%% Calculate symbolic Jacobian
Jsym = simplify(jacobian(P_fin,[q1,q2,q3,q4,q5,q6])); 

%% Pseudo-Inverse-Jacobian based Inverse Kinematics
chk=0;
disp('Desired position:');
disp([xdx;xdy;xdz]);
xd = [xdx;xdy;xdz];
x = PHome;
qi = zeros(6,1);
q_n = zeros(6,length(t));
disp('Processing...');

for idx = 1:length(t)
    % Initial plot (Home config)
    if(idx==1)
        plotarm_rad_3q(qi(1),qi(2),qi(3),idx);
    end
    % Imposing joint limits
    if(abs(qi(1))>2.6179 || abs(qi(2))>2.6179 || abs(qi(3))>2.6179)
        chk=1;
        break;
    else
        J = subs(Jsym,[q1,q2,q3,q4,q5,q6],qi');
        x = subs(T_fin,[q1 q2 q3 q4 q5 q6],qi');
        x = x(1:3,4);
        % Transformation Matrices for each iteration
        j1 = subs(T01,[q1,q2,q3,q4,q5,q6],qi');
        j2 = subs(T02,[q1,q2,q3,q4,q5,q6],qi');
        j3 = subs(T03,[q1,q2,q3,q4,q5,q6],qi');
        j4 = subs(T04,[q1,q2,q3,q4,q5,q6],qi');
        j5 = subs(T05,[q1,q2,q3,q4,q5,q6],qi');
        j6 = subs(T06,[q1,q2,q3,q4,q5,q6],qi');
        j7 = subs(T07,[q1,q2,q3,q4,q5,q6],qi');
        % Joint positions (task space) in each iteration
        jnt1tr(:,idx) = j1(1:3,4);
        jnt2tr(:,idx) = j2(1:3,4);
        jnt3tr(:,idx) = j3(1:3,4);
        jnt4tr(:,idx) = j4(1:3,4);
        jnt5tr(:,idx) = j5(1:3,4);
        jnt6tr(:,idx) = j6(1:3,4);
        jnt7tr(:,idx) = j7(1:3,4);
        % For MATLAB 2015 and greater, use vpa for error
        e = vpa(xd-x);
        % Plotting joint positions of robot
        hold on;
        [~,h,qx,qy,qz] = plotarm_rad_3q(qi(1),qi(2),qi(3),idx);
        if(idx<length(t) && sum(abs(e)<1e-3)<3)
            delete(h);
            delete(qx);
            delete(qy);
            delete(qz);
        end
        if(sum(abs(e)<1e-3) == 3)
            break;
        end
        alpha = 0.1;
        phi = [1;1;1;1;1;1];
        dagJ = J'/(J*J');
        q_n(:,idx) = qi;
        delq = alpha*(dagJ*e + (eye(size(J,2))-dagJ*J)*phi);
        qi = qi + delq;
    end
end
disp('Final End effector position:');
disp(x);
disp('q1 q2 q3 Joint values (in radians) for this position:');
disp(q_n(1:3,idx-1));
if(chk==1) % if joint limits violated
    disp('GOAL STATE NOT REACHABLE.!!!');
elseif (idx==length(t))
    disp(['DID NOT CONVERGE WITHIN ',num2str(length(t)),' ITERATIONS']);
else
    disp(['REACHED GOAL SUCCESSFULLY IN ',num2str(idx),' ITERATIONS.']);
end
% Truncate upto converged iterations
jnt1tr = jnt1tr(:,1:idx);
jnt2tr = jnt2tr(:,1:idx);
jnt3tr = jnt3tr(:,1:idx);
jnt4tr = jnt4tr(:,1:idx);
jnt5tr = jnt5tr(:,1:idx);
jnt6tr = jnt6tr(:,1:idx);
jnt7tr = jnt7tr(:,1:idx);

% Plotting tracked joint positions
% only joints 3 to 7 are plotted here because Joints 1 and 2 are at base
% and do not move. 
figure;
title('Joint Position tracking');
subplot(2,3,1);
plot(1:idx,jnt3tr(1:3,1:idx));
legend('X','Y','Z');
xlabel('iterations');
ylabel('Joint 3 position');

subplot(2,3,2);
plot(1:idx,jnt4tr(1:3,1:idx));
legend('X','Y','Z');
xlabel('iterations');
ylabel('Joint 4 position');

subplot(2,3,3);
plot(1:idx,jnt5tr(1:3,1:idx));
legend('X','Y','Z');
xlabel('iterations');
ylabel('Joint 5 position');

subplot(2,3,4);
plot(1:idx,jnt6tr(1:3,1:idx));
legend('X','Y','Z');
xlabel('iterations');
ylabel('Joint 6 position');

subplot(2,3,5);
plot(1:idx,jnt7tr(1:3,1:idx));
legend('X','Y','Z');
xlabel('iterations');
ylabel('Joint 7 position (End effector)');

% Plot of joint 1,2,3 values
figure;
plot(1:idx-1,q_n(1:3,1:idx-1));
legend('q1','q2','q3');
xlabel('iterations');
ylabel('Joint actuation values');